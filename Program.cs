﻿int FILA = 4, COLUMNA = 4;

//Cuadro de asteriscos
for (int row = 0; row < FILA; row++)
{
    for (int col = 0; col < COLUMNA; col++)
    {
        Console.Write(" * ");
    }
    Console.Write("\n");
}

// Triángulo de asteriscos
for (int row = 0; row < FILA; row++)
{
    for (int col = 0; col < COLUMNA; col++)
    {
        if (col < row)
            Console.Write(" * ");
    }
    Console.Write("\n");
}

Console.WriteLine();

//Rombo de asteriscos
int TAMAÑO = 4;

for (int i = -(TAMAÑO - 1); i < TAMAÑO; i++)
{
    for (int j = 1; j <= Math.Abs(i); j++)
        Console.Write(" ");

    for (int k = 1; k <= TAMAÑO - Math.Abs(i); k++)
        Console.Write("* ");

    Console.WriteLine();
}

//Calcular los números pares que hay en el rango de 1 a 100
int CONTADOR = 1;

while (CONTADOR <= 100)
{
    if (CONTADOR % 2 == 0)
    {
        Console.WriteLine(CONTADOR);
    }
    CONTADOR++;
}

Console.WriteLine();

//Calcular los números impares que hay en el rango de 1 a 100
int CONT = 1;
while (CONT <= 100)
{
    if (CONT % 2 != 0)
    {
        Console.WriteLine(CONT);
    }
    CONT++;
}
